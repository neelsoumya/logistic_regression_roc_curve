logistic_regression_roc_curve

Example code in R to
 
       1) perform logistic regression
       2) perform repeated cross-validation
       3) plot ROC curve, and
       4) plot precision recall curve

 Acknowledgements:
       Adapted from
       
       https://stackoverflow.com/questions/18449013/r-logistic-regression-area-under-curve
       answer by user wush978

 and

	 https://stats.stackexchange.com/questions/10501/calculating-aupr-in-r

 	answer by user arun

	
 Usage:
 
        R --no-save < logistic_regression_roc_curve_withCV.R


 Installation:
 
     install.packages("pROC")
     install.packages("precrec")
     install.packages("PRROC")
